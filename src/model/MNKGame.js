import cloneDeep from 'lodash.clonedeep';
import Matrix from 'matrix-slicer';
import { SQUARE_EMPTY } from '../constants';

export const setSquare = (board, xPosition, yPosition, colorCurrent) => new Promise((resolve) => {
  // TODO: cloneDeep Async
  const newBoard = cloneDeep(board);
  newBoard[xPosition][yPosition] = colorCurrent;
  resolve(newBoard); // fulfilled
});

export const getNextPlayerIndex = (players, currentPlayer) => {
  const player = (currentPlayer + 1) % players.length;
  return player;
};

// TODO: Convert to Promise.all
export const getPotencialLines = (board, chainToWin) => new Promise((resolve) => {
  const matrix = new Matrix(board);
  const lines = [
    ...matrix.getRows(),
    ...matrix.getColumns(),
    ...matrix.getDiagonalsMaj(),
    ...matrix.getDiagonalsMin(),
  ].filter(line => line.length >= chainToWin);
  resolve(lines); // fulfilled
});

// TODO: Convert to Promise.race
export const hasWinner = (potencialLines, chainToWin) => potencialLines.some((potencialLine) => {
  let chainCurrent;

  return potencialLine.some((value, index) => {
    if (index !== 0 && value !== SQUARE_EMPTY && potencialLine[index - 1] === value) {
      chainCurrent += 1;
      // 2 = chain -1
      if (Math.abs(chainCurrent) === chainToWin - 1) {
        return true;
      }
    } else {
      chainCurrent = 0;
    }

    return false;
  });
});
