import initialState from './initialState';
import {
  GAME_RESET,
  GAME_BOARD_UPDATE,
  GAME_NEXT_PLAYER,
  GAME_HAS_WINNER,
  GAME_ADD_MOVE_TO_HISTORY,
} from './actions';

const gameReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GAME_RESET:
      return initialState;
    case GAME_BOARD_UPDATE: {
      return { ...state, board: payload };
    }
    case GAME_NEXT_PLAYER: {
      return { ...state, currentPlayer: payload };
    }
    case GAME_HAS_WINNER: {
      return { ...state, hasWinner: true };
    }
    case GAME_ADD_MOVE_TO_HISTORY: {
      return { ...state, history: [...state.history, payload] };
    }
    default:
      return state;
  }
};

export default gameReducer;
