import { gameSelector, gameSettingsSelector } from './selectors';

import * as MNKGame from '../../model/MNKGame';
import { SQUARE_EMPTY } from '../../constants';

export const GAME_RESET = 'GAME/RESET';
export const GAME_SELECT_SQUARE = 'GAME/SELECT_SQUARE';
export const GAME_BOARD_UPDATE = 'GAME/BOARD_UPDATE';
export const GAME_NEXT_PLAYER = 'GAME/NEXT_PLAYER';
export const GAME_HAS_WINNER = 'GAME/HAS_WINNER';
export const GAME_ERROR = 'GAME/ERROR';
export const GAME_ADD_MOVE_TO_HISTORY = 'GAME/ADD_MOVE_TO_HISTORY';
export const GAME_GO_TO_HISTORY = 'GAME/GO_TO_HISTORY';

export const gameResetAction = () => ({
  type: GAME_RESET,
});

export const gameHasWinner = () => ({
  type: GAME_HAS_WINNER,
});

export const gameError = error => ({
  type: GAME_ERROR,
  payload: error,
});
export const gameAddMoveToHistory = history => ({
  type: GAME_ADD_MOVE_TO_HISTORY,
  payload: history,
});

export const gameNextPlayerAction = () => async (dispatch, getState) => {
  const { currentPlayer, players } = { ...gameSelector(getState()) };
  const state = gameSelector(getState());
  const nextPlayer = MNKGame.getNextPlayerIndex(players, currentPlayer);
  dispatch(gameAddMoveToHistory(state));
  // Recommend by author: https://github.com/reduxjs/redux/issues/1676#issuecomment-215413478
  dispatch({
    type: GAME_NEXT_PLAYER,
    payload: nextPlayer,
  });
};

export const gameUpdateBoardAction = board => async (dispatch, getState) => {
  dispatch({
    type: GAME_BOARD_UPDATE,
    payload: board,
  });
  const { chainToWin } = gameSettingsSelector(getState());
  const potencialLines = await MNKGame.getPotencialLines(board, chainToWin);
  const hasWinner = MNKGame.hasWinner(potencialLines, chainToWin);
  if (hasWinner) {
    dispatch(gameHasWinner());
  } else {
    dispatch(gameNextPlayerAction());
  }
};

export const gameSelectSquareAction = ({ xPosition, yPosition }) => async (
  dispatch,
  getState,
) => {
  const {
    board, currentPlayer, players, hasWinner,
  } = {
    ...gameSelector(getState()),
  };
  const colorCurrent = players[currentPlayer].color;
  if (!hasWinner && board[xPosition][yPosition] === SQUARE_EMPTY) {
    try {
      const newBoard = await MNKGame.setSquare(
        board,
        xPosition,
        yPosition,
        colorCurrent,
      );
      // Code has return <non-promise> in it, then JavaScript automatically wraps it into a resolved promise with that value.
      dispatch(gameUpdateBoardAction(newBoard));
    } catch (err) {
      dispatch(gameError(err));
    }
  }
};
