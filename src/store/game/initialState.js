import Matrix from 'matrix-slicer';
import {
  BOARD_ROW_SIZE,
  BOARD_COLUMN_SIZE,
  PLAYERS_COLORS,
  SQUARE_EMPTY,
  CHAIN_TO_WIN,
} from '../../constants';

const initialState = {
  board: new Matrix(BOARD_COLUMN_SIZE, BOARD_ROW_SIZE, SQUARE_EMPTY).get(),
  players: [
    {
      color: PLAYERS_COLORS[0],
    },
    {
      color: PLAYERS_COLORS[1],
    },
    {
      color: PLAYERS_COLORS[2],
    },
  ],
  hasWinner: false,
  currentPlayer: 0,
  history: [],
  settings: {
    square: { empty: SQUARE_EMPTY },
    board: {
      size: {
        rows: BOARD_ROW_SIZE,
        columns: BOARD_COLUMN_SIZE,
      },
    },
    chainToWin: CHAIN_TO_WIN,
  },
};

export default initialState;
