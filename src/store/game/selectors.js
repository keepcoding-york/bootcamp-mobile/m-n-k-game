import { createSelector } from 'reselect';

export const gameSelector = state => state.game;
export const gameBoardSelector = createSelector(
  gameSelector,
  game => game.board,
);
export const gameCurrentPlayerSelector = createSelector(
  gameSelector,
  game => game.currentPlayer,
);
export const gameHasWinnerSelector = createSelector(
  gameSelector,
  game => game.hasWinner,
);
export const gameHistorySelector = createSelector(
  gameSelector,
  game => game.history,
);
export const gameSettingsSelector = createSelector(
  gameSelector,
  game => game.settings,
);
export const gameSettingsBoardSelector = createSelector(
  gameSettingsSelector,
  settings => settings.board,
);
export const gameSettingsBoardSizeSelector = createSelector(
  gameSettingsBoardSelector,
  board => board.size,
);
export const gameSettingsBoardSizeRowSelector = createSelector(
  gameSettingsBoardSizeSelector,
  size => size.rows,
);
export const gameSettingsBoardSizeColumnSelector = createSelector(
  gameSettingsBoardSizeSelector,
  size => size.columns,
);
