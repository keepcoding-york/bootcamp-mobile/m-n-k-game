import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import localforage from 'localforage';
import { LOCAL_STORE_KEY } from '../constants';
import rootReducer from './reducer';

export { gameResetAction, gameSelectSquareAction } from './actions';

export {
  gameSelector,
  gameBoardSelector,
  gameCurrentPlayerSelector,
  gameSettingsSelector,
  gameSettingsBoardSelector,
  gameSettingsBoardSizeSelector,
  gameSettingsBoardSizeRowSelector,
  gameSettingsBoardSizeColumnSelector,
  gameHasWinnerSelector,
  gameHistorySelector,
} from './selectors';

const persistConfig = {
  key: LOCAL_STORE_KEY,
  storage: localforage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(
  persistedReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(ReduxThunk),
);

const persistor = persistStore(store);

export default {
  store,
  persistor,
};
