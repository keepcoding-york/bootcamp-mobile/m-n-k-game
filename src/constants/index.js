export const LOCAL_STORE_KEY = 'root';
export const BOARD_ROW_SIZE = 5;
export const BOARD_COLUMN_SIZE = 5;
export const SQUARE_EMPTY = 'rgba(255, 255, 255, 0.05)';
export const PLAYERS_COLORS = [
  '#FF1744',
  '#D500F9',
  '#2979FF',
  '#00E5FF',
  '#00E676',
  '#FF9100',
  '#76FF03',
];
export const MAX_PLAYERS = PLAYERS_COLORS.length;
export const MIN_PLAYERS = 2;
export const CHAIN_TO_WIN = 3;
