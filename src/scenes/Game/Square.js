import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Box } from 'rebass';
import styled from 'styled-components';
import { SQUARE_EMPTY } from '../../constants';

const StyledSquare = styled(Box)`
  cursor: pointer;
  position: relative;
  &:before {
    content: '';
    display: block;
    padding-top: 100%;
  }
`;
const StyledPiece = styled(Box)`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  border-radius: 100%;
  opacity: 0;
  transform: scale(0.5);
  transition: transform 300ms, opacity 100ms;
  transition-timing-function: cubic-bezier(0.23, 1, 0.32, 1);
  &.isActive {
    opacity: 1;
    transform: scale(1);
    border-radius: 20%;
  }
`;

export default class Square extends PureComponent {
  handleClick() {
    const { isEmpty } = this.props;
    if (isEmpty) {
      const { selectSquare, x, y } = this.props;
      selectSquare(x, y);
    }
  }

  render() {
    const { width, isEmpty, color } = this.props;
    return (
      <Box
        width={width}
        onClick={() => this.handleClick()}
      >
        <StyledSquare
          m={1 / 2}
          bg={SQUARE_EMPTY}
        >
          <StyledPiece
            bg={!isEmpty && color}
            className={!isEmpty && 'isActive'}
          />
        </StyledSquare>
      </Box>
    );
  }
}

Square.defaultProps = {
  color: SQUARE_EMPTY,
  isEmpty: true,
};

Square.propTypes = {
  width: PropTypes.number.isRequired,
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  color: PropTypes.string,
  isEmpty: PropTypes.bool,
  selectSquare: PropTypes.func.isRequired,
};

// export default Square;
