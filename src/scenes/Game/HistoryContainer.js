import { connect } from 'react-redux';
import History from './History';
import { gameHistorySelector } from '../../store';

const mapStateToProps = state => ({
  history: gameHistorySelector(state),
});

export default connect(mapStateToProps)(History);
