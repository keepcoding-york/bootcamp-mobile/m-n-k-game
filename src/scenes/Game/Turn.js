import React from 'react';
import { Flex, Circle, Text } from 'rebass';
import PropTypes from 'prop-types';
import { PLAYERS_COLORS } from '../../constants';

const Turn = ({ player }) => (
  <Flex
    flexWrap="wrap"
    alignItems="center"
    justifyContent="center"
    p={3}
  >
    <Text
      fontWeight="thin"
      fontSize={4}
    >
      Player
      <Circle
        size={20}
        bg={PLAYERS_COLORS[player]}
        borderRadius={5}
      />
    </Text>
  </Flex>
);

Turn.defaultProps = {
  player: 0,
};

Turn.propTypes = {
  player: PropTypes.number,
};

export default Turn;
