import React from 'react';
import PropTypes from 'prop-types';
import { ButtonOutline, Badge, Text } from 'rebass';

const History = ({ history }) => history.map((value, index) => (
  <div>
    <ButtonOutline
      mb={2}
      fontWeight="light"
      color="#424242"
      border="1px"
      hover={{ bg: 'transparent', color: 'white' }}
    >
      <Text color="#757575">
          Move:
        <Badge bg="#424242">
          {index}
        </Badge>
      </Text>
    </ButtonOutline>
  </div>
));

History.propTypes = {
  history: PropTypes.arrayOf(PropTypes.object),
};

export default History;
