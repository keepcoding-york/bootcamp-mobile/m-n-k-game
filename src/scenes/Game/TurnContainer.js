import { connect } from 'react-redux';
import Turn from './Turn';
import { gameCurrentPlayerSelector } from '../../store';

const mapStateToProps = state => ({
  player: gameCurrentPlayerSelector(state),
});

export default connect(mapStateToProps)(Turn);
