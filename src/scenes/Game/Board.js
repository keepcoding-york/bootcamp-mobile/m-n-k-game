import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Square from './Square';
import { SQUARE_EMPTY } from '../../constants';

export default class Board extends Component {
  coordinatesToIndex(x, y) {
    const { columnSize } = this.props;
    return x + y * columnSize;
  }

  squareWidth() {
    const { columnSize } = this.props;
    return 1 / columnSize;
  }

  squareIsEmpty(value) {
    return value === SQUARE_EMPTY;
  }

  renderSquare(square, rowIndex, columnIndex) {
    const value = square;
    const { gameSelectSquareAction } = this.props;
    const index = this.coordinatesToIndex(rowIndex, columnIndex);
    const squareWidth = this.squareWidth();
    const isEmpty = this.squareIsEmpty(value);
    return (
      <Square
        width={squareWidth}
        key={index}
        color={value}
        isEmpty={isEmpty}
        x={rowIndex}
        y={columnIndex}
        selectSquare={gameSelectSquareAction}
      />
    );
  }

  renderSquares(row, rowIndex) {
    return row.map((square, columnIndex) => this.renderSquare(square, rowIndex, columnIndex));
  }

  renderRows(board) {
    return board.map((row, rowIndex) => this.renderSquares(row, rowIndex));
  }

  render() {
    const { board } = this.props;
    return this.renderRows(board);
  }
}

Board.propTypes = {
  gameSelectSquareAction: PropTypes.func.isRequired,
  board: PropTypes.arrayOf(
    PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  ).isRequired,
  columnSize: PropTypes.number.isRequired,
};
