import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Button, Text } from 'rebass';
import Modal from '../../components/Modal';
import {
  gameHasWinnerSelector,
  gameCurrentPlayerSelector,
  gameResetAction,
} from '../../store';
import Turn from './Turn';

// const mapStateToProps = (state, ownProps) => {
//   console.log(state); // state
//   console.log(ownProps); // ownProps
// };

const ModalWinnerContainer = ({ isActive, playerWinner, gameResetAction }) => (
  <Modal isActive={isActive}>
    <Text fontSize={6}>
Winner is
    </Text>
    <Turn player={playerWinner} />
    <Button
      fontWeight="normal"
      mt={2}
      bg="gray"
      onClick={() => gameResetAction()}
    >
      RESET
    </Button>
  </Modal>
);

ModalWinnerContainer.propTypes = {
  playerWinner: PropTypes.number.isRequired,
  isActive: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  isActive: gameHasWinnerSelector(state),
  playerWinner: gameCurrentPlayerSelector(state),
});

const mapDispatchToProps = dispatch => ({
  gameResetAction: () => dispatch(gameResetAction()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ModalWinnerContainer);
