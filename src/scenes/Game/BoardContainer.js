import { connect } from 'react-redux';
import Board from './Board';
import {
  gameBoardSelector,
  gameResetAction,
  gameSelectSquareAction,
  gameSettingsBoardSizeRowSelector,
  gameSettingsBoardSizeColumnSelector,
} from '../../store';

const mapStateToProps = state => ({
  board: gameBoardSelector(state),
  rowSize: gameSettingsBoardSizeRowSelector(state),
  columnSize: gameSettingsBoardSizeColumnSelector(state),
});

const mapDispatchToProps = dispatch => ({
  gameResetAction: () => dispatch(gameResetAction()),
  gameSelectSquareAction: (xPosition, yPosition) => dispatch(gameSelectSquareAction({ xPosition, yPosition })),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Board);
