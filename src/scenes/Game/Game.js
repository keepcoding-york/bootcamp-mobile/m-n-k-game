import React, { Fragment } from 'react';
import { Flex, Text, Box } from 'rebass';
import styled from 'styled-components';
import Board from './BoardContainer';
import Turn from './TurnContainer';
import History from './HistoryContainer';
import ModalWinnerContainer from './ModalWinnerContainer';

const StyledBoardWrapper = styled(Flex)`
  background-color: ${({ theme }) => theme.colors.white};
`;
const StyledHistoryWrapper = styled.div`
  text-align: left;
`;

const StyledGameWrapper = styled(Flex)`
  width: 100%;
  max-width: 1200px;
  margin: 0 auto;
`;

const Game = () => (
  <Fragment>
    <Box width={1}>
      <Turn />
    </Box>
    <StyledGameWrapper
      flexWrap="wrap"
      alignItems="flex-start"
      justifyContent="center"
    >
      <Box
        width={[1, 'auto']}
        flex="0 1 auto"
      >
        <StyledBoardWrapper
          flexWrap="wrap"
          alignItems="center"
          justifyContent="center"
          width={[1, '40vw']}
        >
          <Board />
        </StyledBoardWrapper>
      </Box>
      <Box
        width={[1, 'auto']}
        flex="0 1 auto"
        p={4}
        pt={[4, 0]}
      >
        <StyledHistoryWrapper>
          <Text
            textAlign="left"
            fontSize={5}
            mb={2}
            fontWeight="thin"
          >
            History
          </Text>
          <History />
        </StyledHistoryWrapper>
      </Box>
    </StyledGameWrapper>
    <ModalWinnerContainer />
  </Fragment>
);

export default Game;
