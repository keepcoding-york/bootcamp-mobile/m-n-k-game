import { SQUARE_EMPTY } from '../constants';

// eslint-disable-next-line
export const createBoardInitial = (rowSize, columnSize) => {
  const matrix = [];
  // eslint-disable-next-line
  let key = 0;
  for (let x = 0; x < rowSize; x += 1) {
    matrix[x] = [];
    for (let y = 0; y < columnSize; y += 1) {
      matrix[x][y] = SQUARE_EMPTY;
      key += 1;
    }
  }
  return matrix;
};
