import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from 'rebass';

const Main = ({ children }) => (
  <Flex flexDirection="column">
    {children}
  </Flex>
);

Main.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Main;
