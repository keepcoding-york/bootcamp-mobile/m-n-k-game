import React from 'react';
import PropTypes from 'prop-types';

const AppContent = ({ children }) => (
  <div className="App">
    {children}
  </div>
);

AppContent.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AppContent;
