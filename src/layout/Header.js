import React from 'react';
import styled from 'styled-components';
import logo from '../images/MNKGame_logo.svg';

const Logo = styled.img`
  width: 100px;
`;

const Header = () => (
  <header className="App-header">
    <Logo
      src={logo}
      alt="Logo"
    />
  </header>
);

export default Header;
