import React, { Component } from 'react';
import { Fixed, Box, Close } from 'rebass';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { transparentize } from 'polished';

const StyledModal = styled(Fixed)`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: center;
  align-content: center;
  align-items: center;
  background: ${transparentize(0.3, '#000')};
  transition: all 150ms ease-in;
  opacity: 0;
  visibility: hidden;
  &.isActive {
    opacity: 1;
    visibility: visible;
  }
`;
const StyledChildrenContent = styled(Box)`
  transform: translateY(40px);
  transition: all 300ms ease-in 50ms;
  opacity: 0;
  visibility: hidden;
  &.isActive {
    transform: translateY(0);
    opacity: 1;
    visibility: visible;
  }
`;

const StyledCloseButton = styled(Close)`
  position: absolute;
  right: 2rem;
  top: 1rem;
`;

export default class Modal extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = { isActive: this.props.isActive };
  // }

  // static getDerivedStateFromProps(props, state) {
  //   return { isActive: props.isActive };
  // }

  closeModal() {
    // this.props.isActive = false;
    this.setState({ isActive: false });
  }

  render() {
    // const { isActive } = this.state;
    const { children, isActive } = this.props;
    return (
      <StyledModal
        zIndex={10}
        right={0}
        left={0}
        top={0}
        bottom={0}
        className={isActive && 'isActive'}
      >
        <StyledCloseButton
          color="white"
          fontSize={6}
          onClick={() => this.closeModal()}
        />
        <StyledChildrenContent
          flex="0 1 auto"
          alignSelf="auto"
          order="0"
          color="white"
          className={isActive && 'isActive'}
        >
          {children}
        </StyledChildrenContent>
      </StyledModal>
    );
  }
}

Modal.defaultProps = {
  isActive: false,
};

Modal.propTypes = {
  children: PropTypes.node.isRequired,
  isActive: PropTypes.bool,
};
