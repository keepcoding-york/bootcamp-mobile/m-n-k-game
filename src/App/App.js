import React from 'react';
import { injectGlobal } from 'styled-components';
import { normalize } from 'polished';
import { Provider as StyleProvider } from 'rebass';
import { Provider as StoreProvider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import storeObj from '../store';
import theme from './theme';
import './App.css';
import AppContent from '../layout/AppContent';
import Main from '../layout/Main';
import Header from '../layout/Header';
import Game from '../scenes/Game/Game';

const { store, persistor } = storeObj;

injectGlobal([
  `
  ${normalize()};
  * { box-sizing: border-box; }
  body { margin: 0; background: #212121; color: #FFF}
  @import url('https://fonts.googleapis.com/css?family=Montserrat:100,300,400,900');
`,
]);

const App = () => (
  <StoreProvider store={store}>
    <PersistGate
      persistor={persistor}
      loading={null}
    >
      <StyleProvider theme={theme}>
        <AppContent>
          <Header />
          <Main>
            <Game />
          </Main>
        </AppContent>
      </StyleProvider>
    </PersistGate>
  </StoreProvider>
);

export default App;
