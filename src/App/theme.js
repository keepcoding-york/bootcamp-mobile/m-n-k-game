const basicColors = {
  black: '#212121',
};

const fonts = {
  sans: 'Montserrat, system-ui, sans-serif',
  mono: 'Menlo, monospace',
};

const fontWeights = {
  thin: 100,
  light: 300,
  normal: 400,
  bold: 700,
};

const theme = {
  colors: {
    ...basicColors,
  },
  fonts: {
    ...fonts,
  },
  fontWeights: {
    ...fontWeights,
  },
};

export default theme;
