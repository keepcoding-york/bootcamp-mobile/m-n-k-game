# Redux

- [Redux](#redux)
    - [Entity creation](#entity-creation)
        - [Create Initial State](#create-initial-state)
        - [Actions (functions entity)](#actions-functions-entity)
        - [Reducer (filter actions)](#reducer-filter-actions)
        - [Selector (split state)](#selector-split-state)
        - [Index (export reducer, actions and selector)](#index-export-reducer-actions-and-selector)
    - [Store creation](#store-creation)
        - [Create Store Initial State](#create-store-initial-state)
        - [Actions (export functions entities)](#actions-export-functions-entities)
        - [Reducer (combineReducers entities)](#reducer-combinereducers-entities)
        - [Selector (export selectors entities states)](#selector-export-selectors-entities-states)
        - [Index (export entities reducers, actions and selectors. Create Store)](#index-export-entities-reducers-actions-and-selectors-create-store)
    - [Component creation](#component-creation)

## Entity creation

    1. Create Initial State
    2. Actions (functions entity)
    3. Reducer (filter actions)
    4. Selector (split state)
    5. Index (export reducer, actions and selector)

### Create Initial State

```js
import { DEFAULT_WORKING_TIME, DEFAULT_RESTING_TIME } from '../../constants';

const initialState = {
  active: false,
  mode: 'working',
  workingTime: 0,
  workingTimeTarget: DEFAULT_WORKING_TIME,
  restingTime: 0,
  restingTimeTarget: DEFAULT_RESTING_TIME,
};

export default initialState;
```

### Actions (functions entity)

```js
export const COUNTER_SET_ACTIVE = 'COUNTER/SET_ACTIVE';
export const COUNTER_SET_MODE = 'COUNTER/SET_MODE';
export const COUNTER_SET_WORKING_TIME = 'COUNTER/SET_WORKING_TIME';
export const COUNTER_SET_RESTING_TIME = 'COUNTER/SET_RESTING_TIME';
export const COUNTER_SET_TARGET = 'COUNTER/SET_TARGET';
export const COUNTER_RESET = 'COUNTER/RESET';

export const counterSetActive = payload => ({
  type: COUNTER_SET_ACTIVE,
  payload,
});

export const counterSetMode = payload => ({
  type: COUNTER_SET_MODE,
  payload,
});

export const counterSetWorkingTime = payload => ({
  type: COUNTER_SET_WORKING_TIME,
  payload,
});

export const counterSetRestingTime = payload => ({
  type: COUNTER_SET_RESTING_TIME,
  payload,
});

export const counterSetTarget = payload => ({
  type: COUNTER_SET_TARGET,
  payload,
});

export const counterReset = () => ({
  type: COUNTER_RESET,
});
```

### Reducer (filter actions)

```js
import {
  COUNTER_SET_ACTIVE,
  COUNTER_SET_MODE,
  COUNTER_SET_WORKING_TIME,
  COUNTER_SET_RESTING_TIME,
  COUNTER_SET_TARGET,
  COUNTER_RESET,
} from './actions';

import initialState from './initialState';

const counterReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case COUNTER_SET_ACTIVE:
      return { ...state, active: payload };
    case COUNTER_SET_MODE:
      return { ...state, mode: payload };
    case COUNTER_SET_WORKING_TIME:
      return { ...state, workingTime: payload };
    case COUNTER_SET_RESTING_TIME:
      return { ...state, restingTime: payload };
    case COUNTER_SET_TARGET:
      const { workingTimeTarget, restingTimeTarget } = payload;
      return { ...state, workingTimeTarget, restingTimeTarget };
    case COUNTER_RESET:
      return initialState;
    default:
      return state;
  }
};

export default counterReducer;
```

### Selector (split state)

```js
import { createSelector } from 'reselect';
import { toClock } from '../../utils';

export const counterSelector = state => state.counter;

export const counterActiveSelector = createSelector(
  counterSelector,
  counter => counter.active,
);

export const counterModeSelector = createSelector(
  counterSelector,
  counter => counter.mode,
);

export const workingTimeSelector = createSelector(
  counterSelector,
  counter => counter.workingTime,
);

export const workingTimeTargetSelector = createSelector(
  counterSelector,
  counter => counter.workingTimeTarget,
);

export const restingTimeSelector = createSelector(
  counterSelector,
  counter => counter.restingTime,
);

export const restingTimeTargetSelector = createSelector(
  counterSelector,
  counter => counter.restingTimeTarget,
);

export const elapsedTimeSelector = createSelector(
  counterModeSelector,
  workingTimeSelector,
  restingTimeSelector,
  (mode, workingTime, restingTime) =>
    mode === 'working' ? workingTime : restingTime,
);

export const targetTimeSelector = createSelector(
  counterModeSelector,
  workingTimeTargetSelector,
  restingTimeTargetSelector,
  (mode, workingTimeTarget, restingTimeTarget) =>
    mode === 'working' ? workingTimeTarget : restingTimeTarget,
);

export const remainingTimeSelector = createSelector(
  elapsedTimeSelector,
  targetTimeSelector,
  (elapsedTimeSelector, targetTimeSelector) =>
    targetTimeSelector - elapsedTimeSelector,
);

export const completedTimeSelector = createSelector(
  elapsedTimeSelector,
  targetTimeSelector,
  (elapsedTime, targetTime) =>
    parseInt((elapsedTime / targetTime) * 100, 10) / 100,
);

export const clockValueSelector = createSelector(
  remainingTimeSelector,
  remainingTime => toClock(remainingTime),
);
```

### Index (export reducer, actions and selector)

```js
export { default as counterReducer } from './reducer';
export {
  counterSetActive,
  counterSetMode,
  counterSetWorkingTime,
  counterSetRestingTime,
  counterSetTarget,
  counterReset,
} from './actions';
export {
  counterSelector,
  counterActiveSelector,
  counterModeSelector,
  workingTimeSelector,
  workingTimeTargetSelector,
  restingTimeSelector,
  restingTimeTargetSelector,
  elapsedTimeSelector,
  targetTimeSelector,
  remainingTimeSelector,
  completedTimeSelector,
  clockValueSelector,
} from './selectors';
```

## Store creation

    1. Create Initial State
    2. Actions (functions entity)
    3. Reducer (filter actions)
    4. Selector (split state)
    5. Index (export reducer, actions and selector)

### Create Store Initial State

```js
import { settingsInitialState } from './settings';

export default {
  settings: settingsInitialState,
};
```

### Actions (export functions entities)

```js
export { settingsSet } from './settings';
export {
  settingsFormSet,
  settingsFormSetWorkingTime,
  settingsFormSetRestingTime,
  settingsFormSetHasSuccess,
} from './settingsForm';
export {
  counterSetActive,
  counterSetMode,
  counterSetWorkingTime,
  counterSetRestingTime,
  counterSetTarget,
  counterReset,
} from './counter';
export { archiveAdd, archiveGet } from './archive';
```

### Reducer (combineReducers entities)

```js
import { combineReducers } from 'redux';

import { settingsReducer } from './settings';
import { settingsFormReducer } from './settingsForm';
import { counterReducer } from './counter';
import { archiveReducer } from './archive';

export default combineReducers({
  settings: settingsReducer,
  settingsForm: settingsFormReducer,
  counter: counterReducer,
  archive: archiveReducer,
});
```

### Selector (export selectors entities states)

```js
export {
  counterSelector,
  counterActiveSelector,
  counterModeSelector,
  workingTimeSelector,
  workingTimeTargetSelector,
  restingTimeSelector,
  restingTimeTargetSelector,
  remainingTimeSelector,
  elapsedTimeSelector,
  targetTimeSelector,
  completedTimeSelector,
  clockValueSelector,
} from './counter';
export {
  settingsSelector,
  settingsInMinSelector,
  workingTimeInMinSelector,
  restingTimeInMinSelector,
} from './settings';
export {
  archiveSelector,
  formattedArchiveSelector,
  archiveLengthSelector,
} from './archive';
```

### Index (export entities reducers, actions and selectors. Create Store)

```js
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import localforage from 'localforage';
import rootReducer from './reducer';

export {
  settingsSet,
  settingsFormSet,
  settingsFormSetWorkingTime,
  settingsFormSetRestingTime,
  settingsFormSetHasSuccess,
  counterSetActive,
  counterSetMode,
  counterSetWorkingTime,
  counterSetRestingTime,
  counterSetTarget,
  counterReset,
  archiveAdd,
  archiveGet,
} from './actions';

export {
  counterSelector,
  counterActiveSelector,
  counterModeSelector,
  workingTimeSelector,
  workingTimeTargetSelector,
  restingTimeSelector,
  restingTimeTargetSelector,
  remainingTimeSelector,
  elapsedTimeSelector,
  targetTimeSelector,
  completedTimeSelector,
  clockValueSelector,
  settingsSelector,
  settingsInMinSelector,
  workingTimeInMinSelector,
  restingTimeInMinSelector,
  archiveSelector,
  formattedArchiveSelector,
  archiveLengthSelector,
} from './selectors';

const persistConfig = {
  key: 'root',
  storage: localforage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(
  persistedReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(ReduxThunk),
);

const persistor = persistStore(store);

export default {
  store,
  persistor,
};
```

## Component creation

Import connect, actions, selector and view

```js
import { connect } from 'react-redux';
import {
  counterSetActive as counterSetActiveACT,
  counterReset as counterResetACT,
  counterActiveSelector,
} from '../../store';
import HomeButtons from './HomeButtons';

const mapDispatchToProps = dispatch => {
  return {
    onClick: value => () =>
      dispatch(value ? counterSetActiveACT(value) : counterResetACT()),
  };
};

const mapStateToProps = state => {
  const active = counterActiveSelector(state);
  return {
    active,
    bg: active ? 'danger' : 'success',
    children: active ? 'Reset' : 'Iniciar',
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeButtons);
```
