# Notes

- https://medium.com/react-native-training/redux-4-ways-95a130da0cdc
- https://github.com/illusionalsagacity/clean-architecture-demo
- https://medium.freecodecamp.org/how-to-write-robust-apps-consistently-with-the-clean-architecture-9bdca93e17b
- https://medium.com/javascript-scene/10-tips-for-better-redux-architecture-69250425af44

## Tutorial

### tic-tac-toe

- https://reactjs.org/tutorial/tutorial.html#wrapping-up
- https://github.com/negomi/react-tic-tac-toe

## Algorithm tic-tac-toe

- https://stackoverflow.com/a/1056352/3410618
- https://stackoverflow.com/a/1610176/3410618

## Algorithm M N K

- http://mathworld.wolfram.com/MagicSquare.html
- https://en.wikipedia.org/wiki/Endgame_tablebase
- https://en.wikipedia.org/wiki/M,n,k-game
- https://stackoverflow.com/questions/1056316/algorithm-for-determining-tic-tac-toe-game-over/1058804#1058804
  - https://stackoverflow.com/a/1058804/3410618
  - https://stackoverflow.com/a/1056342/3410618
- https://stackoverflow.com/questions/4198955/how-to-find-the-winner-of-a-tic-tac-toe-game-of-any-size/34478665
  - https://stackoverflow.com/a/18668901/3410618
  - https://stackoverflow.com/a/4199023/3410618
- https://stackoverflow.com/questions/22593850/how-to-efficiently-detect-a-tie-early-in-m-n-k-game-generalized-tic-tac-toe
- https://www.neverstopbuilding.com/blog/2013/12/13/tic-tac-toe-understanding-the-minimax-algorithm13
- https://medium.freecodecamp.org/how-to-make-your-tic-tac-toe-game-unbeatable-by-using-the-minimax-algorithm-9d690bad4b37

## Redux

- https://medium.com/@ramonvictor/tic-tac-toe-js-redux-pattern-in-plain-javascript-fffe37f7c47a
- https://medium.com/@vanister/learn-react-redux-by-making-a-tic-tac-toe-game-part-1-of-5-dc9111ca09ad
- https://github.com/davidtran/react-redux-tictactoe
- https://medium.com/@marcinbaraniecki/tic-tac-toe-game-in-react-and-redux-155beefa09b0

## React, Inline Functions, and Performance

https://cdb.reacttraining.com/react-inline-functions-and-performance-bdff784f5578

## When to use Component or PureComponent

https://codeburst.io/when-to-use-component-or-purecomponent-a60cfad01a81

## function winner

```js
  lines() {
    const { board } = this.props;
    const matrix = new Matrix(board);

    // 3 = chain
    return [
      ...matrix.getRows(),
      ...matrix.getColumns(),
      ...matrix.getDiagonalsMaj(),
      ...matrix.getDiagonalsMin(),
    ].filter(line => line.length >= 3);
  }

  hasAWinner(lines) {
    return lines.some((line) => {
      let chain;

      return line.some((value, index) => {
        if (index !== 0 && value !== SQUARE_EMPTY && line[index - 1] === value) {
          chain += 1;
          // 2 = chain -1
          if (Math.abs(chain) === 2) {
            return true;
          }
        } else {
          chain = 0;
        }

        return false;
      });
    });
  }
```
